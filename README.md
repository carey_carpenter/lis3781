> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install git
    - Install MYSQL
    - Provide screenshots of installations and ERD
    - Create Bitbucket repos
    - Complete Bitbucket tutoial (BitbucketStationLocations)
    - Provide Bitbucket links
    - Provide Git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Chapter questions
    - SQL code in order to create company and customer tables
    - SQL with indexes and constraints
    - Query result sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Chapter questions
    - SQL code in order to create customer, comodity and order tables
    - Query result sets

4. [P1 README.md](p1/README.md "My p1 README.md file")
    - Screenshots of populated tables for Person table
    - ERD with data
    - Bitbucket links

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - SQL code
    - Screenshot of ERD
    - Bitbucket link

6.  [A5 README.md](a5/README.md "My A5 README.md file")
    - SQL code
    - Screenshot of ERD
    - Screenshot of Query Results from Question 6
    - Bitbucket link