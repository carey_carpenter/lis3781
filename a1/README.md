> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter

### Assignment #1 Requirements:

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Questions
4. Bitbucket repo links:
    a) this assignment
    b) the completed tutorial (BitbucketStationLocations)

#### README.md file should include the following items:

1. Screenshot of installations
2. Screenshot of A1 ERD
3. Bitbucket repos (course and tutorial repos)
4. Git command descriptions


> #### Git commands w/short descriptions:
1. git init – initializes a new local Git repository
2. git status – displays state of repository. Displays tracked and untracked files and changes
3. git add – marks file(s) for staging area; inclusion into next commit. "git add ." stages all files
4. git commit – creates snapshot of repository. Includes timestamps, authors, changes. The -m flag includes a message for the commit. Make commit messages present-tense. Ex) Fix sorting algorithm
5. git push – pushes local commits to remote repository. Ex) git push -u <REMOTENAME> <BRANCHNAME>. After setting upstream, git push can be used
6. git pull – updates local working branch with commits from remote, and updates all remote tracking branches. Run git status before git pull
7. git grep – searches entire repo using a given keyword

#### Assignment Screenshots:

*Screenshot of AAMPS services*
![Development installations screenshot](img/a1_aamps.png)

*Screenshot of ERD*
![Entity Relationship Diagram screenshot](img/a1_erd.png)


#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/carey_carpenter/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A1 Repository Directory](https://bitbucket.org/carey_carpenter/lis3781/src/master/a1/ "A1 Repository Directory")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/carey_carpenter/bitbucketstationlocations/ "Bitbucket Station Locations repository")

*Tutorial: Setting up Distributed Version Control System:*
[A1 Git Setup](http://www.qcitr.com/usefullinks.htm#lesson3b "Git Setup Tutorial")