> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter

### Assignment #2 Requirements:

1. Chapter questions
2. SQL code in order to create company and customer tables
3. SQL with indexes and constraints
4. Query result sets

#### README.md file should include the following items:

1. Screenshots of SQL code
2. Screenshots of populated tables
3. Bitbucket links

#### Assignment Screenshots:

*Screenshots of SQL code*
![Create company](img/create_company.png)
![Insert company](img/insert_company.png)
![Create customer 1](img/create_customer_1.png)
![Create customer 2](img/create_customer_2.png)
![Insert customer](img/insert_customer.png)

*Screenshots of query result sets*
![Company result set](img/company_result_set.png)
![Customer result set](img/customer_result_set.png)


#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/carey_carpenter/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A2 Repository Directory](https://bitbucket.org/carey_carpenter/lis3781/src/master/a2/ "A2 Repository Directory")