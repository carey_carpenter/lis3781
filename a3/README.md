> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter

### Assignment #3 Requirements:

1. Chapter questions
2. SQL code in order to create customer, comodity and order tables
3. Query result sets

#### README.md file should include the following items:

1. Screenshots of SQL code
2. Screenshots of populated tables
3. Query code and screenshots
3. Bitbucket links

#### Assignment Screenshots:

*Screenshots of SQL code*
![A3_code_1](img/A3_code_1.png)
![A3_code_2](img/A3_code_2.png)
![A3_code_3](img/A3_code_3.png)


*Screenshots of query result sets*
![A3_query_result set](img/A3_query_result.png)
![A3_query_result_1 set](img/A3_query_result_1.png)
![A3_query_result_2 set](img/A3_query_result_2.png)


#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/carey_carpenter/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A3 Repository Directory](https://bitbucket.org/carey_carpenter/lis3781/src/master/a3/ "A3 Repository Directory")