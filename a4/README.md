> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter 

### Assignment #4 Requirements:

1. Chapter questions
2. SQL code in order to create tables
3. ERD

#### README.md file should include the following items:

1. SQL code
2. Screenshots of ERD
3. Bitbucket links

#### Assignment Screenshots:

*Screenshots of ERD*
![A4_ERD](img/A4_ERD.png)



#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/carey_carpenter/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A4 Repository Directory](https://bitbucket.org/carey_carpenter/lis3781/src/master/a4/ "A4 Repository Directory")