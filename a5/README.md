# LIS3781 Advanced Database Management

## Carey Carpenter 

### Assignment #5 Requirements:

1. Chapter questions
2. SQL code in order to create tables
3. ERD

#### README.md file should include the following items:

1. SQL code
2. Screenshot of ERD
3. Bitbucket links

#### Assignment Screenshots:

*Screenshot of ERD*
![A5_ERD](img/A5_ERD.png)



#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/carey_carpenter/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A5 Repository Directory](https://bitbucket.org/carey_carpenter/lis3781/src/master/a5/ "A5 Repository Directory")