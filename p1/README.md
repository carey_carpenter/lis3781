> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter

### Project 1 Requirements:

1. Chapter questions
2. SQL code in order to create judge, client and attorney tables
3. ERD with data
4. Query result sets

#### README.md file should include the following items:

1. Screenshots of populated tables for Person table
2. ERD with data
3. Bitbucket links

#### Assignment Screenshots:

*Screenshots of SQL code*
![lis_3781_p1_erd_screenshot](img/lis_3781_p1_erd_screenshot.png)
![Person_table_data_screenshot](img/Person_table_data_screenshot.png)

#### Link to ERD:


[lis3781_ERD](docs/lis3781_p1_erd.mwb)

#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/carey_carpenter/lis3781/src/master/ "Course repository")

*Assignment directory:*
[P1 Repository Directory](https://bitbucket.org/carey_carpenter/lis3781/src/master/p1/README.md "P1 Repository Directory")