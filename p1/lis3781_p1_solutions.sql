drop schema if exists clc20fl;
create schema if not exists clc20fl;
show warnings;
use clc20fl; 

-- Table person 
-- NOTE: allow per_ssn and per_salt to be null, in order to use stored proc CreatePersonSSN below 
DROP TABLE IF EXISTS person; 
CREATE TABLE IF NOT EXISTS person (
    per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT primary key, 
    per_ssn BINARY(64) NULL, 
    per_salt binary(64) NULL COMMENT '*only* demo purposes - do *NOT* use *salt* in the name!', 
    per_fname VARCHAR(25) NOT NULL, 
    per_lname VARCHAR(35) NOT NULL, 
    per_street VARCHAR(30) NOT NULL, 
    per_city VARCHAR(40) NOT NULL, 
    per_state CHAR(2) NOT NULL, 
    per_zip CHAR(9) NOT NULL, 
    per_email VARCHAR(100) NOT NULL,
    per_dob date not null, 
    per_type ENUM('a','c','j') NOT NULL, 
    per_notes VARCHAR(255) NULL,

    UNIQUE INDEX ux_per_ssn (per_ssn ASC) 
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci; 
SHOW WARNINGS;


-- Table attorney
drop table if exists attorney;
create table if not exists attorney (
    per_id smallint unsigned not null primary key,
    aty_start_date date not null,
    aty_end_date date null default null,
    aty_hourly_rate decimal(5,2) not null,
    aty_years_in_practice tinyint not null,
    aty_notes varchar(255) null default null,

    index idx_per_id (per_id asc),

    constraint fk_attorney_person
        foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table client
drop table if exists client;
create table if not exists client (
    per_id smallint unsigned not null primary key,
    cli_notes varchar(255) null default null,

    index idx_per_id (per_id asc),

    constraint fk_client_person
        foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table court
drop table if exists court;
create table if not exists court (
    crt_id tinyint unsigned not null auto_increment primary key,
    crt_name varchar(45) not null,
    crt_street varchar(35) not null,
    crt_city varchar(30) not null,
    crt_state char(2) not null,
    crt_zip char(9) not null,
    crt_phone bigint not null,
    crt_email varchar(100) not null,
    crt_url varchar(100) not null,
    crt_notes varchar(255) null
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table judge
drop table if exists judge;
create table if not exists judge (
    per_id smallint unsigned not null primary key,
    crt_id tinyint unsigned null default null,
    jud_salary decimal(8,2) not null,
    jud_years_in_practice tinyint unsigned not null,
    jud_notes varchar(255) null default null,

    index idx_per_id (per_id asc),
    index idx_crt_id (crt_id asc),

    constraint fk_judge_person
        foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade,

    constraint fk_judge_court
        foreign key (crt_id)
        references court (crt_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table judge_hist
drop table if exists judge_hist;
create table if not exists judge_hist (
    jhs_id smallint unsigned not null auto_increment primary key,
    per_id smallint unsigned not null,
    jhs_crt_id tinyint null,
    jhs_date timestamp not null default current_timestamp,
    jhs_type enum('i', 'u', 'd') not null default 'i',
    jhs_salary decimal(8,2) not null,
    jhs_notes varchar(255) null,

    index idx_per_id (per_id asc),

    constraint fk_judge_hist_judge
        foreign key (per_id)
        references judge (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table 'case'
drop table if exists `case`;
create table if not exists `case` (
    cse_id smallint unsigned not null auto_increment primary key,
    per_id smallint unsigned not null,
    cse_type varchar(45) not null,
    cse_description text not null,
    cse_start_date date not null,
    cse_end_date date null,
    cse_notes varchar(255) null,

    index idx_per_id (per_id asc),

    constraint fk_court_case_judge
        foreign key (per_id)
        references judge (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table bar
drop table if exists bar;
create table if not exists bar (
    bar_id tinyint unsigned not null auto_increment primary key,
    per_id smallint unsigned not null,
    bar_name varchar(45) not null,
    bar_notes varchar(255) null,

    index idx_per_id (per_id asc),

    constraint fk_bar_attorney
        foreign key (per_id)
        references attorney (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table specialty
drop table if exists specialty;
create table if not exists specialty (
    spc_id tinyint unsigned not null auto_increment primary key,
    per_id smallint unsigned not null,
    spc_type varchar(45) not null,
    spc_notes varchar(255) null,

    index idx_per_id (per_id asc),

    constraint fk_speciality_attorney
        foreign key (per_id)
        references attorney (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table assignment
drop table if exists assignment;
create table if not exists assignment (
    asn_id smallint unsigned not null auto_increment primary key,
    per_cid smallint unsigned not null,
    per_aid smallint unsigned not null,
    cse_id smallint unsigned not null,
    asn_notes varchar(255) null,

    index idx_per_cid (per_cid asc),
    index idx_per_aid (per_aid asc),
    index idx_cse_id (cse_id asc),

    unique index ux_per_cid_per_aid_cse_id (per_cid asc, per_aid asc, cse_id asc),
    
    constraint fx_assign_case
        foreign key (cse_id)
        references `case` (cse_id)
        on delete no action
        on update cascade,

    constraint fk_assignment_client
        foreign key (per_cid)
        references client (per_id)
        on delete no action
        on update cascade,

    constraint fk_assignment_attorney
        foreign key (per_aid)
        references attorney (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;


-- Table phone
drop table if exists phone;
create table if not exists phone (
    phn_id smallint unsigned not null auto_increment primary key,
    per_id smallint unsigned not null,
    phn_num bigint unsigned not null,
    phn_type enum('h', 'c', 'w', 'f') not null comment 'home, cell, work, fax',
    phn_notes varchar(255) null,

    index idx_per_id (per_id asc),

    constraint fk_phone_person
        foreign key (per_id)
        references person (per_id)
        on delete no action
        on update cascade
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
show warnings;




--- -----------------------------------------------------
-- Data for table `clc20fl`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester', 'NY', '324402222', 'srogers@comcast.net', '1923-10-03', 'c', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Drive', 'Gotham ', 'NY', '324560000', 'bigtime@gmail.com', '1980-03-10', 'c', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Peter', 'Parker', '654 Airport Drive', 'Gordon', 'Ga', '654560000', 'ilook good@gmail.com', '1980-04-10', 'c', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Jane ', 'Thompson', '118 Eagle Street', 'Atlanta', 'Ga', '342560000', 'yomamma@gmail.com', '1980-05-10', 'c', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Debra ', 'Steele', '1234 Holiday Drive', 'Ft Hood', 'Tx', '234340000', 'youwish@gmail.com', '1980-06-10', 'c', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Tony', 'Stark', '332 Palm Drive', 'Malibu', 'CA', '902638332', 'tstark@gamil.com', '1972-05-04', 'a', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Hank ', 'Pym', '2335 Brown Street', 'Cleveland', 'OH', '903273876', 'hpym@gmail.com', '1980-08-28', 'a', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Bob ', 'Best', '4901 Avondale Avenue', 'Scottsdale', 'AZ', '872349021', 'bbest@yahoo.com', '1986-01-01', 'a', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Sandra ', 'Dole', '87912 Lawrence Ave', 'Atlanta', 'GA', '768972312', 'sdole@gmail.com', '1990-01-26', 'a', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', '562633889', 'bavery@yahoo.com', '1983-12-26', 'a', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid rive', 'Miami', 'FL', '000238762', 'acurry@gmail.com', '1975-12-15', 'j', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Diana ', 'Prince', '944 Green Street', 'Las Vegas', 'NV', '332452893', 'dprice@yahoo.com', '1980-08-22', 'j', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Adam ', 'Jarvis', '98345 Valencia Ave', 'Gulf Shores', 'AL', '870458765', 'ajarvis@gmx.com', '1995-01-31', 'j', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Judy ', 'Sleen', '56754 Rover Ct', 'Billings', 'MT', '672872345', 'jsleen@yahoo.com', '1990-01-03', 'j', NULL);
INSERT INTO `clc20fl`.`person` (`per_id`, `per_ssn`, `per_salt`, `per_fname`, `per_lname`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_email`, `per_dob`, `per_type`, `per_notes`) VALUES (DEFAULT, NULL, NULL, 'Bill', 'Nederman', '43567 Netherland Blvd', 'South Bend', 'IN', '654297651', 'bneiderman@yahoo.com', '1982-02-13', 'j', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`phone`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 1, 8036678827, 'c', NULL);
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 2, 9043327865, 'h', NULL);
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 3, 4439089853, 'h', NULL);
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 4, 1034325598, 'w', 'has two office numbers');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 5, 6402338494, 'w', NULL);
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 6, 4459871034, 'f', 'fax number not working');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 7, 5562981234, 'c', 'prefers home calls');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 8, 1842988765, 'h', NULL);
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 9, 2237651098, 'w', NULL);
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 10, 2652239870, 'f', 'work fax number');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 11, 3348720909, 'h', 'prefers cell calls');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 12, 2321762230, 'w', 'best number to reach at');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 13, 1099987623, 'w', 'call during lunch');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 14, 3348762090, 'c', 'prefers cell calls');
INSERT INTO `clc20fl`.`phone` (`phn_id`, `per_id`, `phn_num`, `phn_type`, `phn_notes`) VALUES (DEFAULT, 15, 9048762893, 'f', 'use for faxing documents');

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`client`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`client` (`per_id`, `cli_notes`) VALUES (1, NULL);
INSERT INTO `clc20fl`.`client` (`per_id`, `cli_notes`) VALUES (2, NULL);
INSERT INTO `clc20fl`.`client` (`per_id`, `cli_notes`) VALUES (3, NULL);
INSERT INTO `clc20fl`.`client` (`per_id`, `cli_notes`) VALUES (4, NULL);
INSERT INTO `clc20fl`.`client` (`per_id`, `cli_notes`) VALUES (5, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`attorney`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (6, '2006-06-12', NULL, 85, 5, NULL);
INSERT INTO `clc20fl`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (7, '2003-08-20', NULL, 130, 28, NULL);
INSERT INTO `clc20fl`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (8, '2009-01-20', NULL, 70, 17, NULL);
INSERT INTO `clc20fl`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (9, '2008-08-01', NULL, 78, 13, NULL);
INSERT INTO `clc20fl`.`attorney` (`per_id`, `aty_start_date`, `aty_end_date`, `aty_hourly_rate`, `aty_years_in_practice`, `aty_notes`) VALUES (10, '2011-09-12', NULL, 60, 14, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`court`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'leon county circuit court', '1767 Airport Road', 'Tallahassee', 'FL', '323035292', 787656778, 'lcc@us.fl.gov', 'http//:www.leoncountycircuitcourt.gov/', NULL);
INSERT INTO `clc20fl`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'leon county traffic court', '276 Holiday Drive', 'Tallahassee', 'Fl', '546570000', 465372856, 'lcc@us.fl.gov', 'http//:www.leoncountytraficcourtt.gov/', NULL);
INSERT INTO `clc20fl`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'florida supreme court', '128 Ward Street', 'Miami', 'FL', '435670000', 455090987, 'fsc@us.fl.gov', 'http//:www.floridasupremecourt.gov/', NULL);
INSERT INTO `clc20fl`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'orange county courthouse', '987 Lane Avenue', 'Gumptown', 'AL', '324530000', 808987654, 'occ@us.fl.gov', 'http//:www.ninthcircuitcourt.gov/', NULL);
INSERT INTO `clc20fl`.`court` (`crt_id`, `crt_name`, `crt_street`, `crt_city`, `crt_state`, `crt_zip`, `crt_phone`, `crt_email`, `crt_url`, `crt_notes`) VALUES (DEFAULT, 'fifth district court', '767 Wayward Drive', 'Dixie', 'GA', '321230000', 706786765, '5dca@us.fl.gov', 'http//:www.5dca.org/', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`judge`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (11, 5, 150000, 10, NULL);
INSERT INTO `clc20fl`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (12, 4, 185000, 3, NULL);
INSERT INTO `clc20fl`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (13, 3, 135000, 2, NULL);
INSERT INTO `clc20fl`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (14, 2, 170000, 6, NULL);
INSERT INTO `clc20fl`.`judge` (`per_id`, `crt_id`, `jud_salary`, `jud_years_in_practice`, `jud_notes`) VALUES (15, 1, 120000, 1, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`case`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 13, 'civil', 'Client says that his logo is being used without his permission to proote a rival business', '2010-09-09', NULL, 'copyright infringement');
INSERT INTO `clc20fl`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 12, 'criminal', 'Client is charged with assaulting her boyfriend during a n argument', '2009-11-18', '2010-12-23', 'assault');
INSERT INTO `clc20fl`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 14, 'civil', 'Client broke an ankle while shopping at a grocery store with no wet floor signs', '2008-05-06', '2008-07-23', 'slip and fall');
INSERT INTO `clc20fl`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 11, 'criminal', 'Client was charged with stealing several tvs at his former job', '2011-05-20', NULL, 'grand theft');
INSERT INTO `clc20fl`.`case` (`cse_id`, `per_id`, `cse_type`, `cse_description`, `cse_start_date`, `cse_end_date`, `cse_notes`) VALUES (DEFAULT, 13, 'criminal', 'Client charged with 10 grams of cocaine found in glove box', '2011-06-05', NULL, 'posession of narcotics');

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`assignment`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 1, 6, 5, NULL);
INSERT INTO `clc20fl`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 2, 7, 4, NULL);
INSERT INTO `clc20fl`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 3, 8, 2, NULL);
INSERT INTO `clc20fl`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 4, 9, 2, NULL);
INSERT INTO `clc20fl`.`assignment` (`asn_id`, `per_cid`, `per_aid`, `cse_id`, `asn_notes`) VALUES (DEFAULT, 5, 6, 5, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`bar`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 6, 'Florida bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 7, 'Alabama bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 8, 'Georgia Bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 9, 'Michigan bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 10, 'South Carolina bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 6, 'Montana bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 7, 'Arizona bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 8, 'Nevada bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 9, 'New York bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 10, 'New York bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 6, 'Georgia Bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 7, 'Alabama bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 8, 'Bay County bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 9, 'Tallahassee bar', NULL);
INSERT INTO `clc20fl`.`bar` (`bar_id`, `per_id`, `bar_name`, `bar_notes`) VALUES (DEFAULT, 10, 'Ocala bar', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`specialty`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 6, 'business', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 7, 'traffic', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 8, 'bankruptcy', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 9, 'insurance', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 10, 'business', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 6, 'judicial', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 7, 'environmental', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 8, 'criminal', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 9, 'real estate', NULL);
INSERT INTO `clc20fl`.`specialty` (`spc_id`, `per_id`, `spc_type`, `spc_notes`) VALUES (DEFAULT, 10, 'malpractice', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clc20fl`.`judge_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `clc20fl`;
INSERT INTO `clc20fl`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 11, 3, '2009-01-16', 'i', 130000, NULL);
INSERT INTO `clc20fl`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 12, 2, '2010-05-23', 'i', 140000, NULL);
INSERT INTO `clc20fl`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 13, 5, '2006-01-18', 'i', 115000, NULL);
INSERT INTO `clc20fl`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 14, 4, '2008-11-16', 'i', 135000, NULL);
INSERT INTO `clc20fl`.`judge_hist` (`jhs_id`, `per_id`, `jhs_crt_id`, `jhs_date`, `jhs_type`, `jhs_salary`, `jhs_notes`) VALUES (DEFAULT, 15, 1, '2006-03-22', 'i', 155000, NULL);

COMMIT;





-- Populate person SSN

-- Populate person table with hashed and salted SSN numbers. *MUST* include salted value in DB! 
DROP PROCEDURE IF EXISTS CreatePersonSSN; 
DELIMITER $$ 
CREATE PROCEDURE CreatePersonSSN() 
BEGIN 
-- MySQL Variables: https://stackoverfIow.com/questions/11754781/how-to-decIare-a-variabIe-in-mysqI 
    DECLARE x, y INT; 
    SET x = 1; 

    -- dynamically set loop ending value (total number of persons) 
    select count(*) into y from person; 
    -- select y; display number of persons (only for testing) 

    WHILE x <= y DO
        -- give each person a unique randomized salt, and hashed and salted randomized SSN. 
        -- Note: this demo is *only* for showing how to include salted and hashed randomized values for testing purposes! 
        SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user when looping 
        SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from111111111 - 999999999, inclusive 
        SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512)); -- each user's SSN value is uniquely randomized and uniquely salted! 

        -- RAND([N]): Returns random floating-point value v in the range O < = v < 1.0 
        -- Documentation: https://dev.mysql.com/doc/refman/5.7/en/mathematical-functions.html#function_rand 

        update person 
        set per_ssn=@ssn, per_salt=@salt 
        where per_id=x;

        SET x = x + 1;
    END WHILE; 
END$$

DELIMITER ; 
call CreatePersonSSN();





-- Create person trigger
select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.';
do sleep(5); 
-- Note: technically, a judge could already be in the person table, yet not added to the judge table. Or, here, add a new person. 
-- add 16th person (a judge), before adding person to judge table (salt and hash per_ssn, and store unique salt, per_salt) 

select 'show person data *before* adding person record' as ''; 
select per_id, per_fname, per_lname from person;
do sleep(5); 

-- give person a unique randomized salt, then hash and salt SSN 
SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user 
SET @num=000000000; --Note: already provided random SSN from111111111 - 999999999, inclusive above
SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000  

INSERT INTO person  
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes) 
values 
(NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge'); 

select 'show person data *after* adding person record' as ''; 
select per_id, per_fname, per_lname from person; 
do sleep(5);

select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trgJudge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

-- Note: use user() rather than current_user()
-- current_user() indicates definer of stored routine/view/trigger/event, not client user

-- after insert of judge table, insert into judge_hist table
drop trigger if exists trg_judge_history_after_insert;

delimiter //
create trigger trg_judge_history_after_insert
after insert on judge
for each row
begin
    insert into judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    values
    (NEW.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary, concat('modifying user: ', user(), ' | notes: ', NEW.jud_notes));
end //
delimiter ;

select 'fire trigger by inserting record into judge table' as '';
do sleep(5);

insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
((select count(per_id) from person), 3, 175000, 3, 'transferred from neighboring jurisdiction');

select 'show judge/judge_hist data *after* AFTER INSERT trigger fires (trg_judge_history_after_insert' as '';
select * from judge;
select * from judge_hist;
do sleep(7);