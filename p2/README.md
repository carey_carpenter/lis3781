> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter

### Project 2 Requirements:

1. Chapter questions
2. Screenshot of required report, and JSON code solution
3. Screenshot of MongoDB shell command

#### README.md file should include the following items:

1. Screenshot of MongoDB shell command
2. Screenshot of required report, and JSON code solution
3. Bitbucket links 

#### Assignment Screenshots:

*Screenshots of SQL code*
![lis_3781_p2_screenshot_1](img/lis_3781_p2_screenshot_1.png)
![lis_3781_p2_screenshot_2](img/lis_3781_p2_screenshot_2.png)
![lis_3781_p2_screenshot_3](img/lis_3781_p2_screenshot_3.png)

#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/carey_carpenter/lis3781/src/master/ "Course repository")

*Assignment directory:*
[P2 Repository Directory](https://bitbucket.org/carey_carpenter/lis3781/src/master/p2/README.md "P2 Repository Directory")