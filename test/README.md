> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Carey Carpenter

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install git
    - Install MYSQL
    - Provide screenshots of installations and ERD
    - Create Bitbucket repos
    - Complete Bitbucket tutoial (BitbucketStationLocations)
    - Provide Bitbucket links
    - Provide Git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

